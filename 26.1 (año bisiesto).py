#Entrada: Año
#Salida: True or False
#¿Que hace?: Comprueba si un año es bisiesto o no,por medio de la división de sus dígitos

año = int(input('Introdusca un año: '))

if año % 4 == 0: #Es la primera condición, si un número es divible entre 4 y no es divisible entre 100 (Si lo es entonces se debe comprobar)
    if año % 100 == 0: #Pero si lo es entoces comprobamos que tambien sea divisible entre 400 y si no lo es, entoces no es bisiesto
        if año % 400 == 0:
            print('Es bisiesto')
        else:
            print('No es bisiesto')
    else:
        print('Si es bisiesto.')
else:
    print('No es bisiesto.')
    
