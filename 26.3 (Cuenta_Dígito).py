#Entrada: un parametro y un número
#Salida: es el número sin los digitos que dicta el parámetro
#¿Que hace?: }Cuenta cuantas veces aparece cuantas veces aparece ese díguito en el número

def cuenta_digito(parametro,numero):
    respuesta = []
    str_parametro = str(parametro)
    string_lista = str(numero)
    lista = list(string_lista)
    contador = 0

    for digit in lista:
        if digit==str_parametro:
            contador += 1

    return(contador)
     


print(cuenta_digito(4,1341544435446))