#Entrada: un parametro y un número
#Salida: es el número sin los digitos que dicta el parámetro
#¿Que hace?: Elimina un díguito que uno define, de un número 

def elimina_digito(parametro,numero):
    respuesta = []
    str_parametro = str(parametro)
    string_lista = str(numero)
    lista = list(string_lista)
    
    for digit in lista:
        if not digit == str_parametro:
            respuesta.append(digit)
    if respuesta == []:
        return "0"
    else:
        string = "" #Definimos un string vacio para añadir el contenido de la lita con el mensaje encriptado
        for i in respuesta: #Sirve para transformar de lista a string  a respuesta
            string+=i
        return string


print(elimina_digito(2,12222222221))