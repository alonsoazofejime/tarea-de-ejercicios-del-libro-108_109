#Entrada:
#Salida:
#¿Que hace?:

def Secuencia_Impares(minimo,maximo):
    lista = []
    contador = minimo

    while contador <= maximo:
        lista.append(contador)
        contador += 1

    lista_impares = []
    for elem in lista:
        if not elem % 2 == 0:
            lista_impares.append(elem)
   
    string = "" #Definimos un string vacio para añadir el contenido de la lita con el mensaje encriptado
    for i in lista_impares: #Sirve para transformar de lista a string  a respuesta
        string += str(i)
    return string

print(Secuencia_Impares(12,16))
